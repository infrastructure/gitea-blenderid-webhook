import dataclasses
import functools
import logging
import os
import re
import urllib.parse
from typing import Optional

import requests
import unidecode

from . import types

_log = logging.getLogger(__name__)

GITEA_URL = os.environ["GITEA_URL"]
GITEA_API_TOKEN = os.environ["GITEA_API_TOKEN"]
GITEA_AUTH_SOURCE_ID = int(os.environ["GITEA_AUTH_SOURCE_ID"])

# HTTP timeout in Gitea communication, in seconds.
GITEA_HTTP_TIMEOUT = int(os.environ.get("GITEA_HTTP_TIMEOUT", "300"))


class GiteaAPIError(Exception):
    """Raised on a Gitea API error, when a more specific exception is not available."""


class InvalidUsernameError(GiteaAPIError):
    pass


class InvalidEmailError(GiteaAPIError):
    pass


class UsernameAlreadyUsedError(GiteaAPIError):
    def __init__(self, username: str) -> None:
        super().__init__()
        self.username = username


def find(
    session: requests.Session,
    bid_userid: int,
) -> Optional[types.GiteaUserInfo]:
    url = _build_api_url("/api/v1/admin/users")

    # Note that returns a list of the users that mtach the parameters.
    resp = session.get(
        url,
        params={
            "login_name": bid_userid,
            "source_id": GITEA_AUTH_SOURCE_ID,
        },
        headers={"Authorization": f"token {GITEA_API_TOKEN}"},
        timeout=GITEA_HTTP_TIMEOUT,
    )

    if resp.status_code != 200:
        raise GiteaAPIError(f"Error calling {url}: {resp.status_code}, {resp.text}")

    payload = resp.json()
    if not payload:
        # Gitea doesn't return an error status when a user is not found. It just
        # returns an empty list as response.
        return None

    if len(payload) > 1:
        _log.warning(
            "Found %d users for this Blender ID account %d! Only updating the first.",
            len(payload),
            bid_userid,
        )
    else:
        _log.info("Found user bid=%d on Gitea", bid_userid)

    # The payload is a list of dicts like this:
    #   {
    #     "id": 3,
    #     "login": "username",
    #     "login_name": "1",
    #     "full_name": "",
    #     "email": "sybren@blender",
    #     "avatar_url": "http://sybren.blender:3001/avatars/cf9529b9dd7e1f2d30388f075b3d106d",
    #     "language": "en-GB",
    #     "is_admin": false,
    #     "last_login": "2023-05-01T12:28:28+02:00",
    #     "created": "2023-05-01T12:28:28+02:00",
    #     "restricted": false,
    #     "active": true,
    #     "prohibit_login": false,
    #     "location": "",
    #     "website": "",
    #     "description": "",
    #     "visibility": "public",
    #     "followers_count": 0,
    #     "following_count": 0,
    #     "starred_repos_count": 0,
    #     "username": "username"
    #   }
    userinfo = payload[0]

    gitea_userinfo = types.GiteaUserInfo(
        source_id=GITEA_AUTH_SOURCE_ID,
        login_name=userinfo["login_name"],
        username=userinfo["username"],
    )
    return gitea_userinfo


def rename(
    session: requests.Session,
    gitea_userinfo: types.GiteaUserInfo,
    bid_nickname: str,
) -> str:
    """Rename a user on Gitea.

    Returns the new username on Gitea, or an empty string if there was a failure
    that can't be retried.
    """
    new_username = gitealize_username(bid_nickname)
    rename_option = types.GiteaRenameUserOption(new_username=new_username)

    # Rename the user on Gitea:
    max_attempts = 1000
    for attempt_num in range(1, max_attempts + 1):
        try:
            _do_rename(session, gitea_userinfo.username, rename_option)
        except InvalidUsernameError:
            _log.error(
                "User bid=%s has invalid username %r, so our gitealize function does not work",
                gitea_userinfo.login_name,
                rename_option.new_username,
            )

            if attempt_num > 0:
                return ""  # Changing digits likely won't help now.
            # The username might become valid when we append a digit, just give that one try.
        except UsernameAlreadyUsedError:
            # This we can handle. Time to try another one in the next iteration.
            _log.info(
                "User bid=%s has invalid already-taken username %r, going to try another one (this was attempt %d)",
                gitea_userinfo.login_name,
                rename_option.new_username,
            )
        else:
            # User edit was OK!
            return rename_option.new_username

        # Construct a new username to try for the next iteration.
        rename_option.new_username = f"{new_username}{attempt_num}"

    _log.error(
        "Could not rename Gitea user %r to %r after %d attempts, giving up",
        gitea_userinfo.username,
        new_username,
        max_attempts,
    )
    return ""


def _do_rename(
    session: requests.Session, username: str, rename_option: types.GiteaRenameUserOption
) -> None:
    """Do the HTTP API request to rename this user."""
    body = dataclasses.asdict(rename_option)
    url = _build_api_url("/api/v1/admin/users", username, "rename")

    resp = session.post(
        url,
        json=body,
        headers={"Authorization": f"token {GITEA_API_TOKEN}"},
        timeout=GITEA_HTTP_TIMEOUT,
    )

    if resp.status_code == 422:  # unprocessable entity
        text = resp.text
        if "invalid username" in text:
            raise InvalidUsernameError(
                f"{text} with username={rename_option.new_username!r}"
            )
        if "[Username]: MaxSize" in text:
            raise InvalidUsernameError(
                f"{text}, too long username={rename_option.new_username!r}"
            )
        if "username is already taken" in text:
            raise UsernameAlreadyUsedError(rename_option.new_username)

    # Gitea should return a 200 with the new user info, but maybe some future
    # version will send a 204, which should also be read as "ok".
    if resp.status_code not in {200, 204}:
        raise GiteaAPIError(f"Error calling {url}: {resp.status_code}, {resp.text}")


def _build_api_url(base_path: str, *args: str) -> str:
    """Return {GITEA_URL}/{base_path}/{url-encoded args}"""
    url_parts = "/".join(urllib.parse.quote(part) for part in args)
    return urllib.parse.urljoin(GITEA_URL, f"{base_path}/{url_parts}")


_validUsernamePattern = re.compile(r"^[\da-zA-Z][.\w-]*$")
# No consecutive or trailing non-alphanumeric chars:
_invalidUsernamePattern = re.compile(r"[._-]{2,}|[._-]$")

_invalidCharsPattern = re.compile(r"[^\da-zA-Z.\w-]+")

# Consecutive non-alphanumeric at start:
_cons_prefix = re.compile(r"^[._-]+")
_cons_suffix = re.compile(r"[._-]+$")
_cons_infix = re.compile(r"[._-]{2,}")

# From models/user/user.go
gitea_reserved_names: set[str] = {
    ".",
    "..",
    ".well-known",
    "admin",
    "api",
    "assets",
    "attachments",
    "avatar",
    "avatars",
    "captcha",
    "commits",
    "debug",
    "error",
    "explore",
    "favicon.ico",
    "ghost",
    "issues",
    "login",
    "manifest.json",
    "metrics",
    "milestones",
    "new",
    "notifications",
    "org",
    "pulls",
    "raw",
    "repo",
    "repo-avatars",
    "robots.txt",
    "search",
    "serviceworker.js",
    "ssh_info",
    "swagger.v1.json",
    "user",
    "v2",
}

# Not yet used, but Gitea has this internally as well:
gitea_reserved_user_patterns = {"*.keys", "*.gpg", "*.rss", "*.atom"}


@functools.lru_cache
def gitealize_username(username: str) -> str:
    """Convert the Phabricator username to something Gitea might accept.

    This is based on IsValidUsername() in modules/validation/helpers.go

    At the moment of writing this code, these transformations will still produce
    unique usernames. This is enforced ih the `UsernameConversionTest` unit
    test in `tests/test_users.py`.
    """

    if not username:
        return ""
    assert isinstance(username, str)

    # Remove accents and other non-ASCIIness.
    ascii_username: str = unidecode.unidecode(username).strip().replace(" ", "_")

    # It is difficult to find a single pattern that is both readable and effective,
    # but it's easier to use positive and negative checks.
    validMatch = _validUsernamePattern.search(ascii_username)
    invalidMatch = _invalidUsernamePattern.search(ascii_username)
    if (
        validMatch
        and not invalidMatch
        and len(ascii_username) <= 40
        and ascii_username not in gitea_reserved_names
    ):
        return ascii_username

    def replace_prefix(match: re.Match[str]) -> str:
        # Earlier idea to convert an N-character prefix into "N" + the first
        # character of the prefix. Just stripping the invalid prefix turned out
        # to be enough and still produces unique usernames.
        #
        # prefix = match.group()
        # return f"{len(prefix)}{prefix[0]}"
        return ""

    def replace_suffix(match: re.Match[str]) -> str:
        return ""

    def replace_infix(match: re.Match[str]) -> str:
        # Replace repeated non-alphanumerics with the single first character of
        # that repeat, so "user_.-name" becomes "user_name"
        infix = match.group()
        return infix[0]

    def replace_illegals(match: re.Match[str]) -> str:
        return "_"

    new_username = ascii_username
    new_username = _invalidCharsPattern.sub(replace_illegals, new_username)
    new_username = _cons_prefix.sub(replace_prefix, new_username)
    new_username = _cons_suffix.sub(replace_suffix, new_username)
    new_username = _cons_infix.sub(replace_infix, new_username)

    if not new_username:
        # Everything was stripped and nothing was left. Better to keep as-is and
        # just let Gitea bork on it.
        return ascii_username

    if new_username in gitea_reserved_names:
        new_username += "2"

    return new_username[:40]
