import dataclasses
from typing import Optional


@dataclasses.dataclass
class UserModified:
    """Blender ID 'user modified' webhook payload."""

    avatar_changed: bool
    confirmed_email_at: str
    date_deletion_requested: Optional[str]
    email: str
    full_name: str
    id: int
    old_email: str
    roles: list[str]
    old_nickname: str
    nickname: str


@dataclasses.dataclass
class GiteaUserInfo:
    """The info needed to identify a user on Gitea."""

    source_id: int  # always 1, meaning to Blender ID.
    login_name: str  # stringified Blender ID user ID.
    username: str  # The current username.


@dataclasses.dataclass
class GiteaUserPatch:
    """The payload sent to Gitea to PATCH the user."""

    source_id: int  # always 1, meaning to Blender ID.
    login_name: str  # stringified Blender ID user ID.
    login: str  # The new username.
    full_name: str
    email: str


@dataclasses.dataclass
class GiteaRenameUserOption:
    """The payload sent to Gitea to rename a user."""

    new_username: str
