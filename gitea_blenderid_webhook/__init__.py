#!/usr/bin/env python

import hashlib
import hmac
import json
import os

from flask import Flask
from flask import request as flask_request
import dotenv
import requests.sessions

# Import .env before import any submodules.
dotenv.load_dotenv()


from . import types, gitea_users


BLENDER_ID_HMAC_SECRET = os.environ["BLENDER_ID_HMAC_SECRET"].encode("ascii")
WEBHOOK_MAX_BODY_SIZE = 10 * 1024  # in bytes

app = Flask(__name__)

app.logger.info(
    "Starting app, GITEA_HTTP_TIMEOUT=%d sec", gitea_users.GITEA_HTTP_TIMEOUT
)


@app.route("/")
@app.route("/hooks/blenderid/")
def index() -> str:
    return "<p><a href='https://www.blender.org/'>Baaaaaye</a></p>"


@app.route("/hooks/blenderid/user-modified", methods=["POST"])
def user_modified() -> tuple[str, int]:
    """Handle user modified request sent by Blender ID."""

    # Check the length of the body
    content_length = int(flask_request.headers["CONTENT_LENGTH"])
    if content_length > WEBHOOK_MAX_BODY_SIZE:
        return "Payload Too Large", 413
    body: bytes = flask_request.data
    if len(body) != content_length:
        return "Content-Length header doesn't match content", 400

    # Validate the request
    mac = hmac.new(BLENDER_ID_HMAC_SECRET, body, hashlib.sha256)
    req_hmac = flask_request.headers.get("X-Webhook-HMAC", "")
    our_hmac = mac.hexdigest()
    if not hmac.compare_digest(req_hmac, our_hmac):
        app.logger.warning(f"Invalid HMAC {req_hmac}")
        return "Invalid HMAC", 400

    # Check the content type
    if flask_request.content_type != "application/json":
        app.logger.info(f"unexpected content type {flask_request.content_type}")
        return "Unsupported Content-Type", 400

    try:
        payload = json.loads(body)
    except json.JSONDecodeError:
        app.logger.exception("Malformed JSON received")
        return "Malformed JSON", 400

    # Explicitly copy each field, so that newly added fields from Blender ID are
    # ignored instead of causing errors.
    user_mod = types.UserModified(
        avatar_changed=payload["avatar_changed"],
        confirmed_email_at=payload["confirmed_email_at"],
        date_deletion_requested=payload["date_deletion_requested"],
        email=payload["email"],
        full_name=payload["full_name"],
        id=payload["id"],
        old_email=payload["old_email"],
        roles=payload["roles"],
        old_nickname=payload.get("old_nickname") or "",
        nickname=payload.get("nickname") or "",
    )

    try:
        handle_user_modified(user_mod)
    except Exception as ex:
        app.logger.exception("error handling webhook")
        return str(ex), 500

    return "", 204


def handle_user_modified(bid_userinfo: types.UserModified) -> None:
    with requests.sessions.Session() as session:
        gitea_userinfo = gitea_users.find(session, bid_userinfo.id)
        if not gitea_userinfo:
            app.logger.info(
                "Found no Gitea user Blender ID account %d", bid_userinfo.id
            )
            return

        if bid_userinfo.nickname != bid_userinfo.old_nickname:
            gitea_users.rename(session, gitea_userinfo, bid_userinfo.nickname)
            app.logger.info("Renamed user bid=%d on Gitea", bid_userinfo.id)
