# Gitea-BlenderID webhook

This Flask project responds to POST requests from Blender ID about user account
changes. It forwards them to the Gitea API.


## VirtualEnv Creation

This project should work on Python 3.9 (and probably also newer).

```sh
python3.9 -m venv venv
. ./venv/bin/activate
pip install poetry
poetry install --without=dev
```

## Environment

Place the following variables in the environment. If there is a `.env` file, it
will be loaded. See the section below on how to obtain the secret & token.

```
BLENDER_ID_HMAC_SECRET=webhook secret of Blender ID
GITEA_URL=https://projects.blender.org/
GITEA_API_TOKEN=API token from Gitea
GITEA_AUTH_SOURCE_ID=database ID of the BlenderID authenticator on Gitea
GITEA_HTTP_TIMEOUT=300  # Optional timeout in second for HTTP comms with Gitea.
```

## Obtaining the Necessary Secrets

This documents the steps necessary on the Blender ID and Gitea sides to hook
them up to this bridge.

### Blender ID side

Add a webhook via https://id.blender.org/admin/.

- Hook type: User Modified
- URL is determined by your installation of this project, typically
  `http://hostname.or.ipaddress:3001/hooks/blenderid/user-modified`
- Secret: put this in `BLENDER_ID_HMAC_SECRET` of your `.env` file.

### Gitea side

Create an API key:

- Log in with an account that has admin rights. This account will become the
  owner of the API key and thus the user associated with the actions
  performed.
- Go to 'Settings' (the personal one, *not* site admin settings), 'Applications':
  `https://your.gitea.url/user/settings/applications`
- Under 'Generate New Token' enter a name you'll recognise, for example
  `blenderid-hook-sudo-scope`.
- If Gitea ever brings back the 'scope' checkboxes, select the 'sudo' scope.
- Click 'Generate Token'.
- Copy the token from the blue box at the top, and put it in the
  `GITEA_API_TOKEN` of your `.env` file.


## Gitea API endpoints used

- `/api/v1/admin/users`: searching for a user with `login_name` = numerical BlenderID
  userID, and `source_id` = the database ID of the Blender ID authenticator.
- `/api/v1/admin/users/{username}/rename`: changing the user's username.

## System configuration

The system-configuration examples below assume that the installation of the blenderid hook has been performed in /usr/local/blenderid/


### User + Directory creation

Create a system-user for blenderid and a directory matching this.
```
adduser --system blenderid
addgroup blenderid


mkdir /usr/local/blenderid
mkdir /usr/local/blenderid/logs

chown -R blenderid:blenderid /usr/local/blenderid
```

Now become blenderid and clone repo into the right spot:
```
sudo su --shell /bin/bash blenderid
cd /usr/local/blenderid/
git clone https://projects.blender.org/infrastructure/gitea-blenderid-webhook.git
```

From there, follow the preparation-steps listed at the top of this README.


### Systemd config to auto-start the hook-handler
The following service-file can be used in `/etc/systemd/system/hooks-blenderid.service`
```
[Unit]
Description=Handler for Blender-id hook

[Service]
User=blenderid
WorkingDirectory=/usr/local/blenderid/gitea-blenderid-webhook
ExecStart=/usr/local/blenderid/gitea-blenderid-webhook/venv/bin/gunicorn -w 3 -b unix:/usr/local/blenderid/gitea-blenderid-webhook/app.sock -m 777 wsgi:app --error-logfile /usr/local/blenderid/logs/blenderid-hook.error --access-logfile /usr/local/blenderid/logs/blenderid-hook.access

[Install]
WantedBy=multi-user.target
```
Before anything else, make sure the path to the logs-dir (in the example above, `/usr/local/blenderid/logs` ) exists before going on.

Be sure to use `systemctl enable hooks-blenderid` and `systemctl start hooks-blenderid`

### Nginx config for port 3001 handling

Using the following snippet in nginx site-definition will make for an easy way to start handling http traffic

```
   server {
        listen 3001;
        server_tokens off;

        location /hooks/blenderid {
          include proxy_params;
          proxy_pass http://unix:/usr/local/blenderid/gitea-blenderid-webhook/app.sock;
        }
    }
```
