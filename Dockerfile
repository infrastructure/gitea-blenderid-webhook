FROM python:3.12.4-alpine

RUN pip install poetry==1.8.3

WORKDIR /app

COPY pyproject.toml poetry.lock wsgi.py ./
COPY gitea_blenderid_webhook ./gitea_blenderid_webhook

RUN poetry install --without dev

EXPOSE 8229
ENV FLASK_APP=wsgi.py

CMD ["poetry", "run", "gunicorn", "-w", "3", "-b", "0.0.0.0:8229", "wsgi:app"]
